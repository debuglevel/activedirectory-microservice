package de.debuglevel.activedirectory

import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.util.*

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GuidUtilsTests {

    @Test
    fun `throws exception if not 16 bytes`() {
        // Arrange

        // Act
        val thrown = Assertions.catchThrowable {
            GuidUtils.guidBytesToUUID(byteArrayOf())
        }

        // Assert
        assertThat(thrown).isInstanceOf(Exception::class.java)
    }

    @Test
    fun `converts to correct UUID`() {
        // Arrange
        val guidBytes = (1..16).map { it.toByte() }.toTypedArray().toByteArray()

        // Act
        val uuid = GuidUtils.guidBytesToUUID(guidBytes)

        // Assert
        // UUID representation was generated from original Java function before converting it to Kotlin
        assertThat(uuid).isEqualTo(UUID.fromString("04030201-0605-0807-090a-0b0c0d0e0f10"))
    }
}