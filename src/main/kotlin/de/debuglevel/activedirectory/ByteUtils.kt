package de.debuglevel.activedirectory

object ByteUtils {
    /**
     * Converts [bytes] to hexadecimal representation.
     *
     * Mainly used for debugging.
     */
    fun bytesToHexString(bytes: ByteArray?): String {
        return bytes?.joinToString(" ") { String.format("%02X", it) } ?: "<empty ByteArray>"
    }
}