package de.debuglevel.activedirectory

import mu.KotlinLogging
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import java.util.*

object LdapUtils {
    private val logger = KotlinLogging.logger {}

    /**
     * Converts an LDAP timestamp to [GregorianCalendar].
     *
     * TODO: Unknown if this respects time zones.
     */
    fun timestampToCalendar(timestamp: Long): GregorianCalendar {
        logger.trace { "Converting LDAP timestamp '$timestamp' to datetime..." }

        val fileTime = timestamp / 10000L - +11644473600000L
        val date = Date(fileTime)
        val calendar = GregorianCalendar()
        calendar.time = date

        logger.trace { "Converted LDAP timestamp '$timestamp' to datetime: $calendar" }
        return calendar
    }

    /**
     * Converts an LDAP timestamp to [GregorianCalendar].
     *
     * TODO: Unknown if this respects time zones.
     */
    fun timestampToCalendar(timestamp: String): GregorianCalendar {
        logger.trace { "Converting LDAP timestamp '$timestamp' to datetime..." }

        val dateTimeFormatter = DateTimeFormatter.ofPattern("uuuuMMddHHmmss[,S][.S]X")
        val zonedDateTime = OffsetDateTime.parse(timestamp, dateTimeFormatter).toZonedDateTime()
        val calendar = GregorianCalendar.from(zonedDateTime)

        logger.trace { "Converted LDAP timestamp '$timestamp' to datetime: $calendar" }
        return calendar
    }
}