package de.debuglevel.activedirectory

import javax.naming.directory.SearchResult

interface EntityActiveDirectoryService<T> {
    /**
     * LDAP Attributes which will be returned on a query
     */
    val returnAttributes: Array<String>

    /**
     * LDAP filter to match a certain group of objects
     *
     * `(&(objectCategory=Person)(objectClass=User))` would be a working base filter on Active Directory to query for persons.
     */
    val baseFilter: String

    /**
     * Name of the entity
     */
    val entityName: String

    /**
     * Builds an LDAP filter for the [searchBy] attribute to match [searchValue].
     */
    fun buildFilter(searchBy: ActiveDirectorySearchScope, searchValue: String): String

    /**
     * Builds a [T] object from the LDAP search result.
     */
    fun build(it: SearchResult): T

    /**
     * Gets all [T] objects.
     */
    fun getAll(): List<T>

    /**
     * Gets all [T] objects where [searchBy] matches [searchValue].
     * Set [searchValue] to `*` to match any.
     */
    fun getAll(searchBy: ActiveDirectorySearchScope, searchValue: String): List<T>

    /**
     * Get the [T] object where [searchBy] matches [searchValue].
     * Set [searchValue] to `*` to match any.
     * Throws [MoreThanOneItemFoundException] if more than one object matches and [NoItemFoundException] if no object matches.
     */
    fun get(searchBy: ActiveDirectorySearchScope, searchValue: String): T

    class NoItemFoundException : Exception("No item found")

    class MoreThanOneItemFoundException(items: List<SearchResult>) :
        Exception("Found more than one result (${items.size} results): $items")

    class InvalidSearchScope(correct: String) : Exception("Invalid SearchScope, use $correct instead.")
}