package de.debuglevel.activedirectory

import java.util.*

// See: https://raw.githubusercontent.com/zagyi/adsync4j/master/core/src/main/java/org/adsync4j/impl/UUIDUtils.java
object GuidUtils {
    /**
     * Converts a byte array into an [UUID] object.
     *
     * Microsoft stores GUIDs in a binary format that differs from the RFC standard of UUIDs (RFC #4122; see details
     * at http://en.wikipedia.org/wiki/Globally_unique_identifier). This function takes [guidBytes] read from Active
     * Directory and correctly decodes it as a [UUID] object.
     */
    fun guidBytesToUUID(guidBytes: ByteArray): UUID {
        if (guidBytes.size != 16) {
            throw IllegalArgumentException("GUID byte array must be exactly 16 bytes")
        }

        var msb = (guidBytes[3].toInt() and 0xFF).toLong()
        msb = msb shl 8 or (guidBytes[2].toInt() and 0xFF).toLong()
        msb = msb shl 8 or (guidBytes[1].toInt() and 0xFF).toLong()
        msb = msb shl 8 or (guidBytes[0].toInt() and 0xFF).toLong()
        msb = msb shl 8 or (guidBytes[5].toInt() and 0xFF).toLong()
        msb = msb shl 8 or (guidBytes[4].toInt() and 0xFF).toLong()
        msb = msb shl 8 or (guidBytes[7].toInt() and 0xFF).toLong()
        msb = msb shl 8 or (guidBytes[6].toInt() and 0xFF).toLong()

        var lsb = (guidBytes[8].toInt() and 0xFF).toLong()
        lsb = lsb shl 8 or (guidBytes[9].toInt() and 0xFF).toLong()
        lsb = lsb shl 8 or (guidBytes[10].toInt() and 0xFF).toLong()
        lsb = lsb shl 8 or (guidBytes[11].toInt() and 0xFF).toLong()
        lsb = lsb shl 8 or (guidBytes[12].toInt() and 0xFF).toLong()
        lsb = lsb shl 8 or (guidBytes[13].toInt() and 0xFF).toLong()
        lsb = lsb shl 8 or (guidBytes[14].toInt() and 0xFF).toLong()
        lsb = lsb shl 8 or (guidBytes[15].toInt() and 0xFF).toLong()

        return UUID(msb, lsb)
    }
}