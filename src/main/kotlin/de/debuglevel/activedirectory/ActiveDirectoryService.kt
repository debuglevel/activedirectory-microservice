package de.debuglevel.activedirectory

import de.debuglevel.activedirectory.EntityActiveDirectoryService.MoreThanOneItemFoundException
import de.debuglevel.activedirectory.EntityActiveDirectoryService.NoItemFoundException
import io.micronaut.context.annotation.Property
import jakarta.inject.Singleton
import mu.KotlinLogging
import javax.naming.directory.SearchControls
import javax.naming.directory.SearchResult
import javax.naming.ldap.Control
import javax.naming.ldap.LdapContext
import javax.naming.ldap.PagedResultsControl
import javax.naming.ldap.PagedResultsResponseControl

@Singleton
class ActiveDirectoryService(
    @Property(name = "app.activedirectory.username") private val username: String,
    @Property(name = "app.activedirectory.password") private val password: String,
    @Property(name = "app.activedirectory.server") private val domainController: String,
    @Property(name = "app.activedirectory.searchbase") private val searchBase: String,
    @Property(name = "app.activedirectory.starttls") private val startTls: Boolean
) {
    private val logger = KotlinLogging.logger {}

    /**
     * Pagination size
     */
    protected val paginationSize = 1000

    /**
     * Builds a [SearchControls] which specifies that only [returnAttributes] will be returned by queries.
     */
    private fun buildSearchControls(returnAttributes: Array<String>): SearchControls {
        return SearchControls().apply {
            searchScope = SearchControls.SUBTREE_SCOPE
            returningAttributes = returnAttributes
        }
    }

    /**
     * Creates a [LdapContext] using [ActiveDirectoryUtils.createLdapContext].
     */
    fun createLdapContext(): LdapContext {
        return ActiveDirectoryUtils.createLdapContext(domainController, username, password, startTls)
    }

    /**
     * Closes the LDAP connection with domain controller.
     */
    protected fun closeLdapContext(ldapContext: LdapContext) {
        logger.debug { "Closing LDAP connection..." }
        ldapContext.close()
    }

    /**
     * Gets all [SearchResult]s matching [filter]. The [SearchResult] will/should only contain [returnAttributes].
     */
    fun getAll(filter: String, returnAttributes: Array<String>): List<SearchResult> {
        logger.debug { "Getting all items for filter='$filter'..." }

        val searchControls = buildSearchControls(returnAttributes)
        val searchResults = mutableListOf<SearchResult>()

        try {
            val ldapContext = createLdapContext()

            // Activate pagination
            var cookie: ByteArray? = null
            ldapContext.requestControls = arrayOf<Control>(PagedResultsControl(paginationSize, Control.NONCRITICAL))

            do {
                logger.debug { "Requesting page..." }

                val pageSearchResults = ldapContext.search(searchBase, filter, searchControls)
                searchResults.addAll(pageSearchResults.toList())

                // Examine the paged results control response
                val controls = ldapContext.responseControls
                if (controls != null) {
                    controls
                        .filterIsInstance<PagedResultsResponseControl>()
                        .forEach { pagedResultsResponseControl ->
                            val resultSize = when {
                                pagedResultsResponseControl.resultSize != 0 -> pagedResultsResponseControl.resultSize.toString()
                                else -> "unknown"
                            }
                            logger.debug { "Page ended (total: $resultSize)" }

                            cookie = pagedResultsResponseControl.cookie
                        }
                } else {
                    logger.debug("No controls were sent from the server")
                }

                // Re-activate paged results
                ldapContext.requestControls =
                    arrayOf<Control>(PagedResultsControl(paginationSize, cookie, Control.CRITICAL))

                logger.debug { "Fetched a total of ${searchResults.count()} entries." }
            } while (cookie != null)

            closeLdapContext(ldapContext)
        } catch (e: Exception) {
            logger.error(e) { "PagedSearch failed." }
            throw e
        }

        logger.debug { "Got ${searchResults.count()} items for filter='$filter'" }
        return searchResults
    }

    /**
     * Get one [SearchResult] matching [filter].
     * The [SearchResult] will/should only contain [returnAttributes].
     * Throws [MoreThanOneItemFoundException] if more than one object matches and [NoItemFoundException] if no object matches.
     */
    fun get(filter: String, returnAttributes: Array<String>): SearchResult {
        logger.debug { "Getting item for filter='$filter'..." }

        val searchResults = getAll(filter, returnAttributes)

        if (searchResults.count() > 1) {
            throw MoreThanOneItemFoundException(searchResults)
        } else if (searchResults.isEmpty()) {
            throw NoItemFoundException()
        }

        val searchResult = searchResults.first()

        logger.debug { "Got item for filter='$filter: $searchResult" }
        return searchResult
    }
}