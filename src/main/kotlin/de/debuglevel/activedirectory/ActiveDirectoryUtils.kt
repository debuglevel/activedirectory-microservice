package de.debuglevel.activedirectory

import de.debuglevel.activedirectory.ByteUtils.bytesToHexString
import mu.KotlinLogging
import java.util.*
import javax.naming.Context
import javax.naming.directory.Attribute
import javax.naming.directory.SearchResult
import javax.naming.ldap.InitialLdapContext
import javax.naming.ldap.LdapContext
import javax.naming.ldap.StartTlsRequest
import javax.naming.ldap.StartTlsResponse


object ActiveDirectoryUtils {
    private val logger = KotlinLogging.logger {}

    /**
     * Creates a domain DN (e.g. `DC=debuglevel,DC=de`) from domain controller name (e.g. `debuglevel.de`).
     */
    fun getBaseDN(domain: String): String {
        logger.debug { "Building base DN for domain '$domain'..." }

        val dn = domain
            .uppercase(Locale.getDefault())
            .split('.')
            .joinToString(",") { "DC=$it" }

        logger.debug { "Built base DN for domain '$domain': '$dn'" }
        return dn
    }

    /**
     * Creates an [LdapContext] by connecting to [domainControllerHost] with the given credentials.
     * If [startTls] is set, the connection will be secured by TLS using StartTLS negotiation.
     * [startTls] must be used if Active Directory Channel Binding is active.
     */
    fun createLdapContext(
        domainControllerHost: String,
        username: String,
        password: String,
        startTls: Boolean
    ): LdapContext {
        logger.debug { "Creating LDAP context (domainController=$domainControllerHost, username=$username, startTls=$startTls)..." }

        val properties = Properties()
        properties[Context.INITIAL_CONTEXT_FACTORY] = "com.sun.jndi.ldap.LdapCtxFactory"
        properties[Context.PROVIDER_URL] = "LDAP://$domainControllerHost"
        properties["java.naming.ldap.attributes.binary"] = "objectGUID"

        return try {
            logger.debug { "Initializing LDAP connection with properties $properties..." }
            val ldapContext = InitialLdapContext(properties, null)

            if (startTls) {
                logger.debug { "Securing connection with StartTLS..." }
                val startTlsResponse = ldapContext.extendedOperation(StartTlsRequest()) as StartTlsResponse
                startTlsResponse.negotiate()
            }

            // CAVEAT: Credentials must be added to the context environment after StartTLS
            ldapContext.addToEnvironment(Context.SECURITY_PRINCIPAL, username)
            ldapContext.addToEnvironment(Context.SECURITY_CREDENTIALS, password)

            ldapContext
        } catch (e: Exception) {
            logger.error(e) { "Initializing LDAP connection failed." }
            throw ConnectionException(e)
        }
    }

    /**
     * Gets the value of [attributeKey]. Returns null if attribute does not exist.
     *
     * TODO: Should we throw an exception if attribute does not exist? For now, null is returned.
     */
    fun SearchResult.getAttributeValue(attributeKey: String): String? {
        logger.trace { "Getting attribute '$attributeKey' value from SearchResult..." }

        val attribute = attributes.get(attributeKey)
        logger.trace { "Got attribute '$attributeKey': $attribute" }
        val value = attribute?.toString()?.substringAfter(": ")

        logger.trace { "Got attribute '$attributeKey' value from SearchResult: $value" }
        return value
    }

    /**
     * Gets the value of [attributeKey] as [ByteArray]. Returns null if attribute does not exist.
     *
     * TODO: Should we throw an exception if attribute does not exist? For now, null is returned.
     */
    fun SearchResult.getBinaryAttributeValue(attributeKey: String): ByteArray? {
        logger.debug { "Getting attribute '$attributeKey' value from SearchResult..." }

        val attribute: Attribute? = attributes.get(attributeKey)
        val byteArray = attribute?.get() as ByteArray?

        logger.debug { "Got attribute '$attributeKey' binary value from SearchResult: ${bytesToHexString(byteArray)}" }
        return byteArray
    }

    /**
     * Gets the value of [attributeKey] as [GregorianCalendar]. Returns null if attribute does not exist.
     *
     * TODO: Should we throw an exception if attribute does not exist? For now, null is returned.
     */
    fun SearchResult.getCalendarAttributeValue(attributeKey: String): GregorianCalendar? {
        logger.debug { "Getting attribute '$attributeKey' value from SearchResult..." }

        val value = this.getAttributeValue(attributeKey)
        val calendar = if (value != null) {
            LdapUtils.timestampToCalendar(value)
        } else {
            null
        }

        logger.debug { "Got attribute '$attributeKey' Calendar value from SearchResult: $calendar" }
        return calendar
    }

    /**
     * Converts a GUID [ByteArray] to an [UUID].
     *
     * TODO: Should we throw an exception if GUID ByteArray is invalid (must be 16 bytes)? For now, null is returned.
     */
    fun toUUID(guidByteArray: ByteArray?): UUID? {
        logger.debug { "Converting GUID ByteArray to UUID..." }

        if (guidByteArray == null) {
            return null
        }

        logger.trace { "Bytes: ${bytesToHexString(guidByteArray)} (${guidByteArray.size})" }
        val uuid = try {
            GuidUtils.guidBytesToUUID(guidByteArray)
        } catch (e: IllegalArgumentException) {
            null
        }
        logger.trace { "UUID: $uuid" }

        logger.debug { "Converted GUID ByteArray to UUID: $uuid" }
        return uuid
    }

    /**
     * Gets the `lastLogon` or `lastLogonTimestamp` attribute value, depending on what is newer.
     * Returns null if none is set.
     *
     * (because one is replicated and the other is not; and even the replication might be delayed by multiple days because of reasons...)
     * See: https://serverfault.com/questions/734615/lastlogon-vs-lastlogontimestamp-in-active-directory
     *
     * TODO: Should we throw an exception if attribute does not exist? For now, null is returned.
     */
    fun getLastLogon(searchResult: SearchResult): GregorianCalendar? {
        val lastLogon = getDatetime(searchResult, "lastLogon")
        val lastLogonTimestamp = getDatetime(searchResult, "lastLogonTimestamp")

        return listOfNotNull(lastLogon, lastLogonTimestamp).maxOfOrNull { it }
    }

    /**
     * Gets a [GregorianCalendar] from the [searchResult] attribute named [attributeKey].
     */
    private fun getDatetime(
        searchResult: SearchResult,
        attributeKey: String
    ): GregorianCalendar? {
        val timestamp = searchResult.getAttributeValue(attributeKey)?.toLong()

        return if (timestamp != null && timestamp != 0L) {
            LdapUtils.timestampToCalendar(timestamp)
        } else {
            null
        }
    }

    class ConnectionException(e: Exception) : Exception("Could not connect to LDAP server", e)
}