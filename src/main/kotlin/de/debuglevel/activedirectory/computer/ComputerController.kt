package de.debuglevel.activedirectory.computer

import de.debuglevel.activedirectory.ActiveDirectoryUtils
import de.debuglevel.activedirectory.EntityActiveDirectoryService
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.security.annotation.Secured
import io.micronaut.security.rules.SecurityRule
import mu.KotlinLogging

/**
 * Generates greetings for persons
 */
@Secured(SecurityRule.IS_AUTHENTICATED)
@Controller("/computers")
class ComputerController(
    private val computerActiveDirectoryService: ComputerActiveDirectoryService
) {
    private val logger = KotlinLogging.logger {}

    @Get("/{name}")
    fun getOne(name: String): HttpResponse<GetComputerResponse> {
        logger.debug("Called getOne($name)")

        return try {
            val computer = computerActiveDirectoryService.get(ComputerSearchScope.Name, name)
            HttpResponse.ok(GetComputerResponse(computer))
        } catch (e: EntityActiveDirectoryService.NoItemFoundException) {
            HttpResponse.notFound(GetComputerResponse(error = "Computer '$name' not found"))
        } catch (e: EntityActiveDirectoryService.MoreThanOneItemFoundException) {
            HttpResponse.serverError(GetComputerResponse(error = "Computer name '$name' is ambiguous"))
        } catch (e: ActiveDirectoryUtils.ConnectionException) {
            HttpResponse.serverError(GetComputerResponse(error = "Could not connect to Active Directory"))
        }
    }

    @Get("/")
    fun getList(): HttpResponse<Set<GetComputerResponse>> {
        logger.debug("Called getList()")

        return try {
            val users = computerActiveDirectoryService.getAll()
                .map {
                    GetComputerResponse(it)
                }.toSet()

            HttpResponse.ok(users)
        } catch (e: ActiveDirectoryUtils.ConnectionException) {
            val response = setOf(GetComputerResponse(error = "Could not connect to Active Directory"))
            HttpResponse.serverError(response)
        }
    }
}