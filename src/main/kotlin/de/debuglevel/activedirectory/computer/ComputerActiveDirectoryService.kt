package de.debuglevel.activedirectory.computer

import de.debuglevel.activedirectory.ActiveDirectorySearchScope
import de.debuglevel.activedirectory.ActiveDirectoryService
import de.debuglevel.activedirectory.ActiveDirectoryUtils.getAttributeValue
import de.debuglevel.activedirectory.ActiveDirectoryUtils.getBinaryAttributeValue
import de.debuglevel.activedirectory.ActiveDirectoryUtils.getCalendarAttributeValue
import de.debuglevel.activedirectory.ActiveDirectoryUtils.getLastLogon
import de.debuglevel.activedirectory.ActiveDirectoryUtils.toUUID
import de.debuglevel.activedirectory.EntityActiveDirectoryService
import jakarta.inject.Singleton
import mu.KotlinLogging
import javax.naming.directory.SearchResult

// see original at https://myjeeva.com/querying-active-directory-using-java.html
@Singleton
class ComputerActiveDirectoryService(
    private val activeDirectoryService: ActiveDirectoryService
) : EntityActiveDirectoryService<Computer> {
    private val logger = KotlinLogging.logger {}

    override val baseFilter = "(&(objectCategory=Computer)(objectClass=Computer))"

    override val entityName = "computer"

    override val returnAttributes =
        arrayOf(
            "cn",
            "userAccountControl",
            "lastLogon",
            "lastLogonTimestamp",
            "whenCreated",
            "logonCount",
            "operatingSystem",
            "operatingSystemVersion",
            "objectGUID"
        )

    override fun getAll(): List<Computer> {
        logger.debug { "Getting all ${entityName}s..." }

        val computers = getAll(ComputerSearchScope.Name, "*")

        logger.debug { "Got ${computers.count()} ${entityName}s" }
        return computers
    }

    override fun getAll(searchBy: ActiveDirectorySearchScope, searchValue: String): List<Computer> {
        logger.debug { "Getting all ${entityName}s with $searchBy='$searchValue'..." }

        val filter = buildFilter(searchBy, searchValue)
        val searchResults = activeDirectoryService.getAll(filter, returnAttributes)
        val computers = searchResults.map { build(it) }

        logger.debug { "Got ${computers.count()} ${entityName}s with $searchBy='$searchValue'" }
        return computers
    }

    override fun get(searchBy: ActiveDirectorySearchScope, searchValue: String): Computer {
        logger.debug { "Getting $entityName $searchBy='$searchValue'..." }

        val filter = buildFilter(searchBy, searchValue)
        val searchResult = activeDirectoryService.get(filter, returnAttributes)
        val computer = build(searchResult)

        logger.debug { "Got $entityName $searchBy='$searchValue': $computer" }
        return computer
    }

    override fun build(it: SearchResult): Computer {
        val commonName = it.getAttributeValue("cn")
        val operatingSystem = it.getAttributeValue("operatingSystem")
        val operatingSystemVersion = it.getAttributeValue("operatingSystemVersion")
        val guid = toUUID(it.getBinaryAttributeValue("objectGUID"))
        val logonCount = it.getAttributeValue("logonCount")?.toInt()
        val userAccountControl = it.getAttributeValue("userAccountControl")?.toIntOrNull()
        val lastLogon = getLastLogon(it)
        val whenCreated = it.getCalendarAttributeValue("whenCreated")

        return Computer(
            commonName,
            userAccountControl,
            logonCount,
            operatingSystem,
            operatingSystemVersion,
            guid,
            lastLogon,
            whenCreated
        )
    }

    override fun buildFilter(searchBy: ActiveDirectorySearchScope, searchValue: String): String {
        logger.debug { "Building filter for searching by '$searchBy' for '$searchValue'..." }

        val filter = when (searchBy) {
            ComputerSearchScope.Name -> "(&($baseFilter)(name=$searchValue))"
            else -> throw EntityActiveDirectoryService.InvalidSearchScope("ComputerSearchScope")
        }

        logger.debug { "Built filter for searching by '$searchBy' for '$searchValue': $filter" }
        return filter
    }
}
