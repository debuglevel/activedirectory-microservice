package de.debuglevel.activedirectory.computer

import java.util.*

data class GetComputerResponse(
    val cn: String? = null,
    val disabled: Boolean? = null,
    val logonCount: Int? = null,
    val operatingSystem: String? = null,
    val operatingSystemVersion: String? = null,
    val guid: UUID? = null,
    val lastLogon: String? = null,
    val whenCreated: String? = null,
    val error: String? = null
) {
    constructor(computer: Computer) : this(
        computer.cn,
        computer.disabled,
        computer.logonCount,
        computer.operatingSystem,
        computer.operatingSystemVersion,
        computer.guid,
        computer.lastLogonFormatted, // TODO: We could probably just use the Calendar, which will be converted to a JSON DateTime. But consuming services must adapt before.
        computer.whenCreatedFormatted // TODO: We could probably just use the Calendar, which will be converted to a JSON DateTime. But consuming services must adapt before.
    )

}