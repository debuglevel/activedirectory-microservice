package de.debuglevel.activedirectory.user

import de.debuglevel.activedirectory.ActiveDirectorySearchScope
import de.debuglevel.activedirectory.ActiveDirectoryService
import de.debuglevel.activedirectory.ActiveDirectoryUtils
import de.debuglevel.activedirectory.ActiveDirectoryUtils.getAttributeValue
import de.debuglevel.activedirectory.ActiveDirectoryUtils.getBinaryAttributeValue
import de.debuglevel.activedirectory.ActiveDirectoryUtils.getCalendarAttributeValue
import de.debuglevel.activedirectory.ActiveDirectoryUtils.getLastLogon
import de.debuglevel.activedirectory.EntityActiveDirectoryService
import jakarta.inject.Singleton
import mu.KotlinLogging
import javax.naming.directory.SearchResult

// see original at https://myjeeva.com/querying-active-directory-using-java.html
@Singleton
class UserActiveDirectoryService(
    private val activeDirectoryService: ActiveDirectoryService,
) : EntityActiveDirectoryService<User> {
    private val logger = KotlinLogging.logger {}

    override val baseFilter = "(&(objectCategory=Person)(objectClass=User))"

    override val entityName = "user"

    override val returnAttributes =
        arrayOf(
            "sAMAccountName",
            "givenName",
            "sn",
            "cn",
            "mail",
            "displayName",
            "userAccountControl",
            "lastLogon",
            "lastLogonTimestamp",
            "whenCreated",
            "objectGUID",
        )

    override fun getAll(): List<User> {
        logger.debug { "Getting all ${entityName}s..." }

        val users = getAll(UserSearchScope.Username, "*")

        logger.debug { "Got ${users.size} ${entityName}s" }
        return users
    }

    override fun getAll(searchBy: ActiveDirectorySearchScope, searchValue: String): List<User> {
        logger.debug { "Getting all ${entityName}s with $searchBy='$searchValue'..." }

        val filter = buildFilter(searchBy, searchValue)
        val searchResults = activeDirectoryService.getAll(filter, returnAttributes)
        val users = searchResults.map { build(it) }

        logger.debug { "Got ${users.size} ${entityName}s with $searchBy='$searchValue'" }
        return users
    }

    override fun get(searchBy: ActiveDirectorySearchScope, searchValue: String): User {
        logger.debug { "Getting $entityName $searchBy='$searchValue'..." }

        val filter = buildFilter(searchBy, searchValue)
        val searchResult = activeDirectoryService.get(filter, returnAttributes)
        val user = build(searchResult)

        logger.debug { "Got $entityName $searchBy='$searchValue': $user" }
        return user
    }

    override fun build(it: SearchResult): User {
        val samaaccountname = it.getAttributeValue("samaccountname")!!
        val givenname = it.getAttributeValue("givenname")
        val mailaddress = it.getAttributeValue("mail")
        val commonName = it.getAttributeValue("cn")
        val surname = it.getAttributeValue("sn")
        val displayName = it.getAttributeValue("displayName")
        val userAccountControl = it.getAttributeValue("userAccountControl")?.toIntOrNull()
        val guid = ActiveDirectoryUtils.toUUID(it.getBinaryAttributeValue("objectGUID"))
        val lastLogon = getLastLogon(it)
        val whenCreated = it.getCalendarAttributeValue("whenCreated")

        return User(
            samaaccountname,
            givenname,
            mailaddress,
            commonName,
            surname,
            displayName,
            userAccountControl,
            lastLogon,
            whenCreated,
            guid
        )
    }

    override fun buildFilter(searchBy: ActiveDirectorySearchScope, searchValue: String): String {
        logger.debug { "Building filter for searching by '$searchBy' for '$searchValue'..." }

        val filter = when (searchBy) {
            UserSearchScope.Email -> "(&($baseFilter)(mail=$searchValue))"
            UserSearchScope.Username -> "(&($baseFilter)(samaccountname=$searchValue))"
            else -> throw EntityActiveDirectoryService.InvalidSearchScope("UserSearchScope")
        }

        logger.debug { "Built filter for searching by '$searchBy' for '$searchValue': $filter" }
        return filter
    }
}
