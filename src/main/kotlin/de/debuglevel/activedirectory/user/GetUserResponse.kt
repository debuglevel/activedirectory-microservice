package de.debuglevel.activedirectory.user

import java.util.*

data class GetUserResponse(
    val username: String? = null,
    val givenname: String? = null,
    val mail: String? = null,
    val cn: String? = null,
    val sn: String? = null,
    val displayName: String? = null,
    val disabled: Boolean? = null,
    val lastLogon: String? = null,
    val guid: UUID? = null,
    val whenCreated: String? = null,
    val error: String? = null
) {
    constructor(user: User) : this(
        user.username,
        user.givenname,
        user.mail,
        user.cn,
        user.sn,
        user.displayName,
        user.disabled,
        user.lastLogonFormatted, // TODO: We could probably just use the Calendar, which will be converted to a JSON DateTime. But consuming services must adapt before.
        user.guid,
        user.whenCreatedFormatted // TODO: We could probably just use the Calendar, which will be converted to a JSON DateTime. But consuming services must adapt before.
    )

}