package de.debuglevel.activedirectory.user

import de.debuglevel.activedirectory.ActiveDirectoryUtils
import de.debuglevel.activedirectory.EntityActiveDirectoryService
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.security.annotation.Secured
import io.micronaut.security.rules.SecurityRule
import mu.KotlinLogging

/**
 * Generates greetings for persons
 */
@Secured(SecurityRule.IS_AUTHENTICATED)
@Controller("/users")
class UserController(
    private val userActiveDirectoryService: UserActiveDirectoryService
) {
    private val logger = KotlinLogging.logger {}

    @Get("/{username}")
    fun getOne(username: String): HttpResponse<GetUserResponse> {
        logger.debug("Called getOne($username)")

        return try {
            val user = userActiveDirectoryService.get(UserSearchScope.Username, username)
            HttpResponse.ok(GetUserResponse(user))
        } catch (e: EntityActiveDirectoryService.NoItemFoundException) {
            HttpResponse.notFound(GetUserResponse(error = "User '$username' not found"))
        } catch (e: EntityActiveDirectoryService.MoreThanOneItemFoundException) {
            HttpResponse.serverError(GetUserResponse(error = "Username '$username' is ambiguous"))
        } catch (e: ActiveDirectoryUtils.ConnectionException) {
            HttpResponse.serverError(GetUserResponse(error = "Could not connect to Active Directory"))
        }
    }

    @Get("/")
    fun getList(): HttpResponse<Set<GetUserResponse>> {
        logger.debug("Called getList()")

        return try {
            val users = userActiveDirectoryService.getAll()
                .map {
                    GetUserResponse(it)
                }.toSet()

            HttpResponse.ok(users)
        } catch (e: ActiveDirectoryUtils.ConnectionException) {
            val response = setOf(GetUserResponse(error = "Could not connect to Active Directory"))
            HttpResponse.serverError(response)
        }
    }
}